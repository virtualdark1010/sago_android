package com.sagas.sagas

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class SagasApp : Application()
package com.sagas.sagas.presentation.modules.org_receipt_pallet_express

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.sagas.sagas.R

class OrgReceiptPalletExpressActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_org_receipt_pallet_express)
    }
}
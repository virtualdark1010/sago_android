package com.sagas.sagas.presentation.modules.shipment_pick_and_drop

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.sagas.sagas.R

class ShipmentPickAndDropActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shipment_pick_and_drop)
    }
}
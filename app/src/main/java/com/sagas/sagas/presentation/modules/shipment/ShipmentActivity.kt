package com.sagas.sagas.presentation.modules.shipment

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.sagas.sagas.R

class ShipmentActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shipment)
    }
}
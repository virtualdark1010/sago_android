package com.sagas.sagas.presentation.modules.acl_printing_issue_auto

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.sagas.sagas.R

class ACLPrintingIssueAutoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_aclprinting_issue_auto)
    }
}
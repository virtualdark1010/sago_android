package com.sagas.sagas.presentation.modules.sub_inventory_transfer

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.sagas.sagas.R

class SubInventoryTransferActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sub_inventory_transfer)
    }
}